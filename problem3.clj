(ns euler.problem2)

(defn foo[] (println "foo")

(defn fib
  ([] (cons 1 (cons 2 (fib 1 2))))
  ([a b]
     (lazy-seq (cons (+ a b) (fib b (+ a b))))))

(reduce + (filter even? (take-while #(< % 4000000) (fib))))


(def fibs (lazy-cat '(0 1) (map + fibs (drop 1 fibs))))

  (take 10 fibs)

  (map + [1 2 3 4 5] (drop 1 [1 2 3 4 5]))

