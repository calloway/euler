(ns euler.problem17)


(def dig1 {0 "",  1 "one", 2 "two", 3 "three", 4 "four", 5 "five", 6 "six", 7 "seven", 8 "eight", 9 "nine"})
(def dig10 {10 "ten", 11 "eleven", 12 "twelve", 13 "thirteen", 14 "fourteen", 15 "fiveteen", 16 "sixteen", 17 "seventeen", 18 "eighteen", 19 "nineteen"})

(def dig2 {2 "twenty", 3 "thirty", 4 "fourty", 5 "fifty", 6 "sixty", 7 "seventy", 8 "eighty", 9 "ninety"})

(defn <100-words [[a b :as ls]]
  (condp = ls
    [0 b] `("" ~(dig1 b))
    [1 0] '("ten" "")
    [1 1] '("eleven" "")
    [1 2] '("twelve" "")
    [1 3] '("thirteen" "")
    [1 5] '("fifteen")
    [1 8] '("eighteen")
    [1 b] `(~(dig1 b) "teen")
    [4 b] `("forty" ~(dig1 b))
    [a b] `(~(dig2 a) ~(dig1 b))
    [a] `(~(dig1 a))
    "else"))

(<100-words [5 0])
(defn >100-words [[a b c d :as ls]]
  (condp = ls
    [1 0 0 0] '("one" "thousand")
    [a 0 0] `(~(dig1 a) "hundred")
    [a b c]  (flatten  (conj (<100-words [b c]) `(~(dig1 a) "hundred" "and")))
    [a b] (<100-words [a b])
    [a] (<100-words [a])
    (str  "else" ls)))

(reduce +  (map count  (>100-words [1 1 5])))

(<100-words [1 6])

(defn words-out [n]
 (mapcat #(>100-words (to-ls %)) (drop 1 ( range (inc n)))))

(defn number-count [n]
  (reduce + (map count (mapcat #(>100-words (to-ls %)) (drop 1 ( range (inc n)))))))

(number-count 1000)

(reduce + (map count (number-count 1000)))

(def dig (merge dig1 dig10))

(defn to-ls 
  ( [n] (to-ls n '()))
  ( [n ls] 
      (if (zero? (quot n 10))
        (conj ls (rem n 10))
        (recur (quot n 10) (conj ls (rem n 10))))))

(to-ls 1234)
