(ns euler.problem7
    (:require [clojure.math.numeric-tower :refer [expt sqrt]]))

(defn prime? [n] 
  (cond
   (< n 2) false
   (< n 4) true
   :else
   (nil? (some #(zero? (rem n %)) (range 2 (inc (sqrt n)))))))

(def primes (filter prime? (range)))

(prime? 0)
(take 6 primes)
(last (take 10001 primes))
