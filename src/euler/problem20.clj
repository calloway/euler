(ns euler.problem20)

(defn n! [n]
  (reduce * (range 1 (inc n))))

(defn sum-digits 
  ([n] (sum-digits n 0))
  ([n acc]
     (if (< n 10)
       (+ n acc)
       (sum-digits (quot n 10) (+ acc (mod n 10) )))))

(sum-digits (n! 100))
  
(n! 100)

(n! 10)
(mod 235 10)
(quot 235 10)
