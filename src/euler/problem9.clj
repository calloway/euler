(ns euler.problem9
  (:require [clojure.math.numeric-tower :refer [expt sqrt]]))

(reduce * (first (take 1
                       (filter  (fn [[a b c]] (=  (* c c) (+ (* b b) (* a a))))
                                (filter (fn [[a b c]] (= (+ a b c) 1000)) (for [c (range 998) b (range c) a (range b)]
                                                                           [a b c]
                                                                           ))))))

