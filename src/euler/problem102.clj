(ns euler.problem102
  (:require [clojure.java.io :refer [resource]] 
            [clojure.string :refer [split]]
            [clojure.math.numeric-tower :refer [sqrt abs]]))

(defn sqr [x] (* x x))

(defn distance[[x1 y1] [x2 y2]]
  (sqrt (+ (sqr (- y1 y2)) (sqr (- x1 x2)))))

(defn create-triangle [input]
  (partition 2 (map read-string (split input #","))))

(defn distance-sides [[v1 v2 v3]]
  "Return A B C"
  `(~(distance v1 v2) ~(distance v2 v3) ~(distance v1 v3)))

(defn area 
  "http://www.mathopenref.com/heronsformula.html"
  [triangle]
  (let [[A B C] (distance-sides triangle)
        p (/ (+ A B C) 2)]
    (sqrt (* p (- p A) (- p B) (- p C)))))


(defn contained? [triangle]
  "Point is contained if area of subtriangles with origin as vertex is approximately =
Area of the entire triangle"
  (let [totalarea (area triangle)
        origin [0 0]
        area12 (area [origin (first triangle) (second triangle)])
        area23 (area [origin (second triangle) (last triangle)])
        area31 (area [origin (last triangle) (first triangle)])
        approximately= #(< (abs (- %1 %2)) 0.001)]
    (approximately= totalarea (+ area12 area23 area31))))

(map create-triangle (split (slurp (resource "triangles.txt")) #"\n"))

(let [ triangles (map create-triangle (split (slurp (resource "triangles.txt")) #"\n"))]
  (count  (filter contained? triangles)))

