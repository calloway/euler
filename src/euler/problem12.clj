(ns euler.problem12)

(def tri
  ((fn rtri [a b]
     (lazy-seq (cons (+ a b) (rtri (+ a b) (inc b))))
     ) 0 1))

(defn prime-factors 
  ([n] (prime-factors n [])) 
  ([n factors]
     (if (= n 1)
       factors
       (let [factor (first (filter #(zero? (rem n %)) (drop 2 (range (inc n)))))]
         (prime-factors (quot n factor) (conj factors factor))))))

(first (filter #(zero? (rem 3 %)) (drop 2  (range 3))))

(quot 6 2)

(prime-factors 28)

(defn divisors>? [x]
  (fn [n]
    (> (num-divisors n) x)))


(time
(first (filter (divisors>? 500) tri)))

(take 10 tri)
;(take 1 (filter  #((every-pred divisors>5? tri?) %) (range)))
