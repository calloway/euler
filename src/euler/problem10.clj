(ns euler.problem10
  (:require [clojure.math.numeric-tower :refer [expt sqrt]]))


(defn prime? [n] 
  (cond
   (< n 2) false
   (< n 4) true
   :else (nil? (some #(zero? (rem n %)) (range 2 (inc (sqrt n)))))))

(prime? 0)
(def primes (filter prime? (range)))

(take-while #(< % 1000) primes)

(reduce + (take-while #(< % 2000000) primes))
