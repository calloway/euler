(ns euler.problem23)

(defn divisors [n]
  (filter #(zero? (rem n %)) (range 1 (inc (quot n 2)))))

(defn abundant? [n]
  (> (reduce + (divisors n)) n))

(let [numbers (filter abundant? (range 1 28124))
      total (reduce + (range 28124))]
  (->> (for [x numbers y numbers] (+ x y))
       (filter #(< % 28124))
       (into #{})
       (reduce +)
       (- total)))
