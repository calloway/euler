(ns euler.problem25)

(def fib ((fn rfib [fn2 fn1 term]
            (lazy-seq (cons {:term term :num fn1} (rfib fn1 (+' fn1 fn2) (inc term))))) 0 1 1) )

(defn numdigits [n]
  (count (seq (str n))))


((first (filter #(= (numdigits (% :num)) 1000) fib)) :term)

(def fib-seq
  (lazy-cat [1 1] (map + (rest fib-seq) fib-seq)))

(take 20 fib-seq)
