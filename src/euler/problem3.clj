(ns euler.problem3
  (:require [clojure.math.numeric-tower :refer [expt sqrt]]))

(defn prime? [n] 
  (if (< n 4)
    true
    (nil? (some #(zero? (rem n %)) (range 2 (inc (sqrt n)))))))


(defn primefactors [n]
  (filter #(and (zero? (rem n %)) (prime? %)) (drop 2 (range (inc (quot n 2))))))
(primefactors 14)

(defn lp [n]
  (if (prime? n)
    n
    (recur (quot n (first (primefactors n))))
    ))
(lp 600851475143)
