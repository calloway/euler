(ns euler.problem1)

(letfn [(+multiples [m n]
          (reduce + (range 0 n m))

          )]
  (- (+ (+multiples 3 1000) (+multiples 5 1000))
     (+multiples 15 1000)))

