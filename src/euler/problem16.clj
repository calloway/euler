(ns euler.problem16
  (:require [clojure.math.numeric-tower :refer [expt sqrt]]))


(reduce + (map (comp #(- % 48) int) (seq (str (expt 2 1000)))))

(reduce + (map (comp #(- % 48) int) (seq (str (reduce *' (repeat 1000 2))))))

