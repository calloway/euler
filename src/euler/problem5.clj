(ns euler.problem5
    (:require [clojure.math.numeric-tower :refer [expt sqrt]]))


(defn foo [n]
  (not-any? #(not (zero? (mod n %))) (range 1 11)))

(first (filter foo (drop 1 (range))))
(foo 2520)

(range 1 11)
(defn prime? [n] 
  (if (< n 4)
    true
    (nil? (some #(zero? (rem n %)) (range 2 (inc (sqrt n)))))))


(defn primefactors [n]
  (filter #(and (zero? (rem n %)) (prime? %)) (drop 2 (range (inc (quot n 2))))))
(defn lp [n]
  (if (prime? n)
    n
    (recur (quot n (first (primefactors n))))
    ))

(defn myf [a b]
  (do
    (println "a: " a ", b: " b ", rem: " (rem a b))
    (if (zero? (rem a b))
      a
      (* a (lp b)))))

(reduce myf 1 (range 1 5))

(reduce myf 1 (range 1 21))

  

